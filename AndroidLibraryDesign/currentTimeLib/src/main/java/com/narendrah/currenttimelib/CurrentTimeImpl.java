package com.narendrah.currenttimelib;

public interface CurrentTimeImpl {
    /**
     * This method will be called by Current time
     * class on every time current time is fetched.
     *
     * @param time the current time
     */
    void setCurrentTimeToView(String time);
}
