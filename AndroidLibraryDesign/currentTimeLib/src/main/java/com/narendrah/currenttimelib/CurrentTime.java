package com.narendrah.currenttimelib;

import android.os.RemoteException;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;

public class CurrentTime implements Runnable {
    private CurrentTimeImpl currentTimeImpl;
    public CurrentTime(CurrentTimeImpl ct) {
        this.currentTimeImpl = ct;
    }

    @Override
    public void run() {
        while (true) {

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Date currentTime = Calendar.getInstance().getTime();
            String time = currentTime.toString();
            currentTimeImpl.setCurrentTimeToView(time);
        }
    }
}
