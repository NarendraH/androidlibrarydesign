package com.narendrah.timeapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.narendrah.currenttimelib.CurrentTime;
import com.narendrah.currenttimelib.CurrentTimeImpl;

public class MainActivity extends AppCompatActivity implements CurrentTimeImpl {
    public static String TAG = MainActivity.class.getSimpleName();
    private TextView tvTime;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvTime = (TextView) findViewById(R.id.tvTimeText);

    }

    /**
     * @param view
     */
    public void getCurrentTime(View view) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                CurrentTime currentTime = new CurrentTime(MainActivity.this);
                currentTime.run();
            }
        });
        thread.start();
    }

    /**
     * @param time the current time
     */
    @Override
    public void setCurrentTimeToView(final String time) {
        Log.d(TAG, "setCurrentTimeToView: " + time);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvTime.setText(time);
            }
        });
    }
}